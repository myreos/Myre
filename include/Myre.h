/*
** Myre.h for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Mon Feb 20 07:55:47 2017 grandr_r
** Last update Wed Jul 26 08:18:28 2017 grandr_r
*/

#ifndef MYRE_H_
#define MYRE_H_

#include <efi.h>
#include <efilib.h>

/* string utils.c */

EFI_STATUS      putstring(CHAR16 *str);
void            memset(void *str, char c, int len);
int             is_printable(CHAR16 c);
int		strcmp(CHAR16 *s1, CHAR16 *s2);
CHAR16		*strcpy(CHAR16 *dest, CHAR16 *src);
void		putchar(CHAR16 c);
void		putnbr(int nb);
CHAR16		*strncpy(CHAR16 *dest, CHAR16 *src, int n);
int		strlen(CHAR16 *s);
int     	strlen16(CHAR16 *s);
void		memcpy(void *dest, void *src, unsigned int len);
int             strncmp(CHAR16 *s1, CHAR16 *s2, int n);
CHAR16		*strcat(CHAR16 *dest, CHAR16 *src);
CHAR16		*strdup(CHAR16 *s);
int		atoi(CHAR16 *s);
char		match(CHAR16 *s, CHAR16 *s2);
char    	match_case(CHAR16 *s1, CHAR16 *s2);
CHAR16		*epur_str(CHAR16 *s1, CHAR16 sep);

/* efi_utils.c */

void		ResetKey(EFI_INPUT_KEY *key);
char		file_exists(EFI_FILE *base_dir, CHAR16 *filename);
CHAR16          *get_full_path(CHAR16 *current_dir, CHAR16 *filename);
char            is_directory(EFI_FILE *base_dir, CHAR16 *filename);
UINT8           get_directory_last_occurency(CHAR16 *str);

/* shell.c */

#define NB_CMD 21

// TODO : if split .h in several files : remove this

typedef struct		s_link
{
  CHAR16		*data;
  struct s_link		*next;
  struct s_link		*prev;
}			t_link;

typedef struct		s_cmd
{
  UINT8			pos;
  CHAR16		line[100];
  char			command_finished;
}			t_cmd;

typedef struct		s_env
{
  CHAR16		**env;
  EFI_INPUT_KEY		key;
  EFI_HANDLE		base_image;
  EFI_LOADED_IMAGE	*loaded_image;
  EFI_FILE		*base_dir;
  EFI_FILE_HANDLE	log_file;
}			t_env;

typedef struct		s_shell
{
  void 			(*char_func[127])(struct s_shell *shell);
  void 			(*key_func[25])(struct s_shell *shell);
  void          	(*cmd_ptr[NB_CMD])(struct s_shell *, CHAR16 **);
  t_cmd			*cmd;
  char			loop;
  CHAR16		*command_list[NB_CMD];
  t_env			*env;
  CHAR16		*current_dir;
  t_link		*history;
}			t_shell;

t_cmd			*init_cmd();
EFI_STATUS		shell_loop(t_env *env);
void			free_shell(t_shell *shell);

/* keyboard.c */

void            init_keyboard_ptr(void (**char_func)(t_shell *shell), void (**key_func)(t_shell *shell));

/* commands.c */

void            interpret_command(t_shell *shell, CHAR16 *cmd);
void            graphics_start(t_shell *shell, CHAR16 **args);
void            list(t_shell *shell, CHAR16 **args);
void            select(t_shell *shell, CHAR16 **args);
void            help(t_shell *shell, CHAR16 **args);
void            clear(t_shell *shell, CHAR16 **args);
void            ls(t_shell *shell, CHAR16 **args);
void            boot(t_shell *shell, CHAR16 **args);
void            shutdown(t_shell *shell, CHAR16 **args);
void            reboot(t_shell *shell, CHAR16 **args);
void            exit(t_shell *shell, CHAR16 **args);
void            echo(t_shell *shell, CHAR16 **args);
CHAR16          **listing(EFI_FILE_HANDLE currdir);
void		history(t_shell *shell, CHAR16 **args);
void		cat(t_shell *shell, CHAR16 **args);
void		update_myre(t_shell *shell, CHAR16 **args);

/* array_utils.c */

CHAR16          **str_to_array(CHAR16 *str, char sep);
void    	show_array(CHAR16 **arr);
void    	free_array(CHAR16 **arr);
int		arraylen(CHAR16 **arr);
UINTN		array_memory_space(CHAR16 **arr);
CHAR16          **realloc_array(CHAR16 **arr, UINT8 size);
void            sort_array(CHAR16 **arr);

/* line_utils.c */

void            erase_line(t_shell *shell);
void            remove_char(t_shell *shell, int pos);
void            print_line(t_shell *shell);
void            insert_char(t_shell *shell, CHAR16 c);

/* ls.c */

typedef struct 			_EFI_LIST_ENTRY {
  struct _EFI_LIST_ENTRY	*Flink;
  struct _EFI_LIST_ENTRY	*Blink;
}				EFI_LIST_ENTRY;

/* env.c */

void		getenv(t_shell *shell, CHAR16 **args);
CHAR16		*getenv_cmd(t_env *env, CHAR16 *request);
void		env(t_shell *shell, CHAR16 **args);
void		add_to_env(t_env *env, CHAR16 *str);
void		setenv(t_shell *shell, CHAR16 **args);
void		unsetenv(t_shell *shell, CHAR16 **args);
t_env   	*init_env(EFI_HANDLE image);
void		free_env(t_env *env);
void		unset(t_env *env, CHAR16 *str);

/* read_config.c */

typedef struct	s_file
{
  UINT8		*buffer;
  UINTN		size;
}		t_file;

void	read_config(t_env *env, CHAR16 *filename);
t_file	*read_file(EFI_FILE_HANDLE file);
CHAR16	*get_line(UINT8 *buffer, UINTN buffer_size, UINTN *pos);
void	verify_config(t_env *env);

/* graphics.c */

typedef struct			s_img
{
  UINTN 			height;
  UINTN				width;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL	*data;
}				t_img;

#define NB_CMD_GRAPHICS 5

typedef	struct			s_graphics
{
  EFI_GRAPHICS_OUTPUT_PROTOCOL	*screen;
  UINTN				height;
  UINTN				width;
  t_env				*env;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL	*pixels;
  char				loop;
  UINT8				index;
  UINT8				nb_entry;
  t_img				*background;
  t_img				**icons;
  void				(*ptr_func[NB_CMD_GRAPHICS])(struct s_graphics *);
  t_img				**icons_drawn;
  t_img				**icons_drawn_up;
  t_img				**icons_drawn_mid;
  //t_img				**original_icons;
}				t_graphics;

EFI_STATUS	graphics_loop(t_env *env);
void            draw_icons(t_graphics *screen);
void            moving_animation(t_graphics *screen);

/* load_bmp.c */

#pragma pack(1)

typedef	struct			s_bmp
{
  CHAR8				b;
  CHAR8				m;
  UINT32			size;
  UINT16			reserved[2];
  UINT32			image_offset;
  UINT32			header_size;
  UINT32			pixel_width;
  UINT32			pixel_height;
  UINT16			planes;
  UINT16			bit_per_pixel;
  UINT32			compression_type;
  UINT32			image_size;
  UINT32			xpixels_per_meter;
  UINT32			ypixels_per_meter;
  UINT32			number_of_colors;
  UINT32			important_colors;
}				t_bmp;

#pragma pack()

t_img   *read_bmp(t_file *file);

/* entries.c */

UINT8           get_nb_entry(t_env *env);
CHAR16          *get_entry_icon(t_env *env, UINT8 nb_entry);
void            entry_to_env(t_env *env, CHAR16 *entry);
CHAR16          *get_entry_path(t_env *env, UINT8 i);
CHAR16          *format_path(CHAR16 *path);
CHAR16          *get_entry_name(t_env *env, UINT8 i);

/* images.c */

t_img                   *init_image(UINTN height, UINTN width);
void                    free_image(t_img *image);
t_img                   *read_from_png(t_env *env, CHAR16 *filename);
t_img                   *resize_image(t_img *img, UINTN height, UINTN width);
void            	draw_image_at(UINTN startx, UINTN starty, t_graphics *screen, t_img *image);
t_img                   *copy_img(t_img *image);
t_img                   *copy_pixels_zone(UINTN startx, UINTN starty, UINTN width, UINTN height, t_graphics *screen);

/* list.c */

#define NB_CMD_LIST 4

typedef struct		s_list
{
  t_env			*env;
  UINT8			index;
  char 			loop;
  UINT8			nb_entry;
  void          	(*ptr_func[NB_CMD_LIST])(struct s_list *);
}			t_list;

EFI_STATUS      list_loop(t_env *env);

/* folder_utils.c */

void            cd(t_shell *shell, CHAR16 **args);
void		pwd(t_shell *shell, CHAR16 **args);

/* linked_list.c */

void            free_link(t_link *root);
void            show_link(t_link *list);
void		push_head(t_link *list, CHAR16 *elem);
void            push_tail(t_link *list, CHAR16 *elem);
void            push_before(t_link *list, CHAR16 *elem);
void            push_after(t_link *list, CHAR16 *elem);
t_link          *init_link(CHAR16 *data);

/* io.c */

void		log(t_env *env, CHAR16 *message);

/* autoscan.c */

void		autoscan_entries(t_env *env);

/* other strings read only */

#define GREETINGS L"******************************* Welcome to Myre *******************************\r\n\
***            Please refer to help if needed (type help in shell)          ***\r\n\
*******************************************************************************\r\n"
#define MYRE_TEXT L"******************* Welcome to Myre ******************\r\n\r\n"
#define BASE_ENV L"DEFAULT_DIR=\\:USERNAME=Myre_user:CONFIG=default:MODE=shell:DEFAULT_ENTRY=1"
#define PROMPT L"[grandr_r@myre]> "
#define CMD_LEN 100
#define SCALE_MULTIPLIER 2097152ULL

#endif
