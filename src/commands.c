/*
** commands.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Tue Feb 21 15:12:26 2017 grandr_r
** Last update Wed Jul 26 07:55:04 2017 grandr_r
*/

#include <Myre.h>

void			update_myre(__attribute__((unused)) t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  Print(L"Updating Myre...\r\n");
}

t_file			*cat_open(t_shell *shell, CHAR16 *filename)
{
  CHAR16		*full_path;
  EFI_FILE_HANDLE	handle;
  EFI_STATUS		stat;
  t_file		*file;

  full_path = get_full_path(shell->current_dir, filename);
  stat = shell->env->base_dir->Open(shell->env->base_dir, &handle, full_path, EFI_FILE_MODE_READ, 0);
  if (EFI_ERROR(stat))
    {
      Print(L"Error while opening: %s: %r\r\n", filename, stat);
      return (NULL);
    }
  file = read_file(handle);
  FreePool(full_path);
  handle->Close(handle);
  return (file);
}

void			cat(t_shell *shell, CHAR16 **args)
{
  t_file		*file;
  CHAR16		*line;
  UINTN			pos;

  if (arraylen(args) == 1)
    {
      Print(L"Error : Usage : %s file\n", args[0]);
      return;
    }
  if ((file = cat_open(shell, args[1])) == NULL)
    return;
  pos = 0;
  while ((line = get_line(file->buffer, file->size, &pos)))
    {
      //Print(L"Line getted, pos = %d\r\n", pos);
      Print(L"%s\r\n", line);
      FreePool(line);
      //Print(L"Line freed\r\n");
    }
  FreePool(file->buffer);
  FreePool(file);
}

void			history(t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  show_link(shell->history);
}

void			graphics_start(t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  log(shell->env, L"Starting graphics...\n");
  if (strcmp(getenv_cmd(shell->env, L"MODE="), L"graphics") == 0)
    {
      shell->loop = 0;
      return;
    }
  ResetKey(&shell->env->key);
  graphics_loop(shell->env);
  shell->loop = 0;
}

void			list(t_shell *shell, CHAR16 **args)
{
  CHAR16		**list;
  CHAR16		*entries;
  int			i;

  i = 0;
  if (arraylen(args) < 2)
    {
      Print(L"Error : Usage : %s obj_to_list\n", args[0]);
      return;
    }
  if (strcmp(args[1], L"entry") == 0)
    {
      entries = strdup(getenv_cmd(shell->env, L"ENTRY_LIST="));
      list = str_to_array(entries, '|');
      Print(L"(Entry No): (Bootloader Name):(Bootloader Path)\r\n");
      while (list[i])
	{
	  Print(L"%hs%d: %s\r\n", L"Entry #", i + 1, list[i]);
	  i++;
	}
      //show_array(list);
      free_array(list);
      FreePool(entries);
    }
  else
    Print(L"Error: %s: Unknow list object\r\n", args[1]);
}

void			select_entry_by_num(t_shell *shell, CHAR16 **args)
{
  CHAR16		*entry;
  CHAR16		**entry_list;
  CHAR16		**entry_splitted;
  int			entry_no;

  entry_no = atoi(args[2]) - 1;
  entry = strdup(getenv_cmd(shell->env, L"ENTRY_LIST="));
  entry_list = str_to_array(entry, '|');
  if (entry_no + 1 > arraylen(entry_list) || entry_no + 1 <= 0)
    {
      Print(L"Error: Entry #%d: no such entry\r\n", entry_no + 1);
      FreePool(entry);
      free_array(entry_list);
      return;
    }
  entry_splitted = str_to_array(entry_list[entry_no], ':');
  format_path(entry_splitted[1]);
  /* TODO : Should be deleted when config file will be securised by verifing if
     entry exists or not */
  if (file_exists(shell->env->base_dir, entry_splitted[1]))
    entry_to_env(shell->env, entry_splitted[1]);
  else
    Print(L"Could not select \"%s\" : File not found\r\n", entry_splitted[1]);
  free_array(entry_list);
  free_array(entry_splitted);
  FreePool(entry);
}

void			select(t_shell *shell, CHAR16 **args)
{
  CHAR16		*full_path;

  if (arraylen(args) < 2)
    {
      Print(L"Error : Usage : %s efi_to_select\n", args[0]);
      return;
    }
  else if (arraylen(args) > 2 && strcmp(L"entry", args[1]) == 0)
    {
      select_entry_by_num(shell, args);
      return;
    }
  else if (arraylen(args) > 2)
    {
      Print(L"Error: Unknow option for %s: %s\r\n", args[0], args[1]);
      return;
    }
  if (!file_exists(shell->env->base_dir, args[1]))
    full_path = get_full_path(shell->current_dir, args[1]);
  else
    full_path = strdup(args[1]);
  format_path(full_path);
  if (file_exists(shell->env->base_dir, full_path))
    entry_to_env(shell->env, full_path);
  else
    Print(L"Could not select \"%s\" : File not found\r\n", full_path);
  FreePool(full_path);
}

void			init_help_cmd(CHAR16 **help_cmd)
{
  help_cmd[0] = L"Simple exit command to quit the shell and return to firmware interface";
  help_cmd[1] = L"Echo builtin, $> echo arg to display arg or echo $env_var to display env_var content.\r\nEquivalent to getenv env_var";
  help_cmd[2] = L"Shutdown the computer";
  help_cmd[3] = L"Reboot the computer";
  help_cmd[4] = L"List file in current directory";
  help_cmd[5] = L"Boot the efi file choosen using select (see help select)";
  help_cmd[6] = L"Display the current environnement variables";
  help_cmd[7] = L"Get environnement variable specified in parameter";
  help_cmd[8] = L"Set a new environnement variable or replace an old one";
  help_cmd[9] = L"Remove an environnement variable";
  help_cmd[10] = L"Clear the terminal";
  help_cmd[11] = L"Select an efi file to boot from\r\nYou could either choose with direct path (Ex: /efi/boot/Myre/BOOTX64.efi\r\nOr even by entry lunmber (see list entry for the list)";
  help_cmd[12] = L"Display this help";
  help_cmd[13] = L"List the specified object. List of objects are :\r\n-entry\r\n";
  help_cmd[14] = L"Start the graphic mode, if you havn't tried yet, give it a try !";
  help_cmd[15] = L"Change the current directory (default directory is specified in Myre.cfg file";
  help_cmd[16] = L"Show the current directory";
  help_cmd[17] = L"Display the history of typed commands\r\nUse history -c in order to clean history";
  help_cmd[18] = L"Output the content of file passed as parameter";
  help_cmd[19] = L"Update Myre to it's last version";
  help_cmd[NB_CMD - 1] = L"Error this is not a valid function name";
}

int			find_arg(t_shell *shell, CHAR16 *arg)
{
  int			i;

  i = 0;
  while (shell->command_list[i])
    {
      if (strcmp(shell->command_list[i], arg) == 0)
	return (i);
      i++;
    }
  return (i);
}

void			help(t_shell *shell, CHAR16 **args)
{
  int			i;
  CHAR16		*help_cmd[NB_CMD];

  i = 0;
  if (arraylen(args) == 1)
    {
      Print(L"Here is the list of available function for this shell : \r\n");
      while (shell->command_list[i])
	{
	  Print(L"%s\r\n", shell->command_list[i]);
	  i++;
	}
      Print(L"Consider \"help function_name\" to get help for a specific function\r\n");
    }
  else
    {
      init_help_cmd(help_cmd);
      Print(L"%s\r\n", help_cmd[find_arg(shell, args[1])]);
    }
}

void			clear(__attribute__((unused)) t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  ST->ConOut->ClearScreen(ST->ConOut);
}

CHAR16			**listing(EFI_FILE_HANDLE currdir)
{
  UINTN			size;
  VOID			*buffer;
  EFI_FILE_INFO		*info;
  EFI_STATUS		stat;
  UINT8			end_of_listing;
  CHAR16		**array;

  end_of_listing = 0;
  array = AllocatePool(2 * sizeof(CHAR16 *));
  array[0] = strdup(L"");
  array[1] = NULL;
  //TODO : proper listing (may be find out how to color string)
  while (end_of_listing != 1)
    {
      size = 256;
      buffer = AllocatePool(size);
      stat = currdir->Read(currdir, &size, buffer);
      if (EFI_ERROR(stat) || size == 0)
	end_of_listing = 1;
      else
	{
	  info = (EFI_FILE_INFO *)buffer;
	  array = realloc_array(array, arraylen(array) + 1);
	  array[arraylen(array)] = strdup(info->FileName);
	  //Print(L"%s\r\n", info->FileName);
	}
      FreePool(buffer);
    }
  sort_array(array);
  return (array);
}

/*void			ls_list(t_shell *shell, CHAR16 **list)
{
  EFI_FILE_INFO		*info;
  UINT8			i;
  CHAR16		*filename;
  EFI_FILE_HANDLE       dir;
  EFI_FILE_HANDLE       currfile;

  dir = LibOpenRoot(shell->env->loaded_image->DeviceHandle);
  i = 0;
  while (list[i])
    {
      filename = get_full_path(shell->current_dir, list[i]);
      dir->Open(dir, &currfile, filename, EFI_FILE_MODE_READ, 0);
      info = LibFileInfo(currfile);
      if ((info->Attribute & EFI_FILE_DIRECTORY))
	Print(L"%t   %es\r\n", info->ModificationTime, info->FileName);
      else
	Print(L"%t   %hs\r\n", info->ModificationTime, info->FileName);
      FreePool(info);
      FreePool(filename);
      currfile->Close(currfile);
      i++;
    }
}*/

void			ls(t_shell *shell, CHAR16 **args)
{
  EFI_FILE_HANDLE	dir;
  EFI_FILE_HANDLE	currfile;
  CHAR16		**list;

  dir = LibOpenRoot(shell->env->loaded_image->DeviceHandle);
  dir->Open(dir, &currfile, shell->current_dir, EFI_FILE_MODE_READ, 0);
  list = listing(currfile);
  if (arraylen(args) == 1)
    show_array(list);
  //if (arraylen(args) == 2 && strcmp(args[1], L"-l") == 0)
  //ls_list(shell, list);
  //free_array(list);
  currfile->Close(currfile);
}

void			boot(t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  EFI_HANDLE		image;
  EFI_DEVICE_PATH 	*path;
  EFI_STATUS		err;
  CHAR16		*filename;

  filename = getenv_cmd(shell->env, L"SELECTED_EFI=");
  log(shell->env, L"Booting : ");
  log(shell->env, filename);
  log(shell->env, L" : ");
  if (filename == NULL)
    {
      Print(L"Please first choose an efi file to boot from (see help select)\r\n", filename);
      log(shell->env, L"fail\n");
      return;
    }
  Print(L"Booting %s\r\n", filename);
  path = FileDevicePath(shell->env->loaded_image->DeviceHandle, filename);
  if (!path)
    {
      Print(L"Error on path for %s\r\n", filename);
      log(shell->env, L"fail\n");
      return;
    }
  err = uefi_call_wrapper(BS->LoadImage, 6, FALSE, shell->env->base_image, path, NULL, 0, &image);
  if (EFI_ERROR(err))
    {
      Print(L"Error loading %s: %r\r\n", filename, err);
      log(shell->env, L"fail\n");
      return;
    }
  log(shell->env, L"done\n");
  free_env(shell->env);
  err = uefi_call_wrapper(BS->StartImage, 3, image, NULL, NULL);
  if (EFI_ERROR(err))
    {
      Print(L"Error Booting %s: %r", filename, err);
      return;
    }
  shell->loop = 0;
}

void		shutdown(t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  log(shell->env, L"Shutting down the system...\n");
  //free_shell(shell);
  free_env(shell->env);
  //free_array(args);
  RT->ResetSystem(EfiResetShutdown, EFI_SUCCESS, 0, NULL);
}

void		reboot(t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  log(shell->env, L"Rebooting the system...\n");
  //free_shell(shell);
  free_env(shell->env);
  //free_array(args);
  RT->ResetSystem(EfiResetCold, EFI_SUCCESS, 0, NULL);
}

void		exit(t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  shell->loop = 0;
}

void		echo(__attribute__((unused)) t_shell *shell, CHAR16 **args)
{
  int		i;

  i = 1;
  while (args[i])
    {
      Print(args[i]);
      if (args[i + 1])
	putchar(' ');
      i++;
    }
  Print(L"\r\n");
}

void		interpret_command(t_shell *shell, CHAR16 *cmd)
{
  CHAR16	**args;
  int		i;

  if (cmd[0] == 0)
    return;
  i = 0;
  args = str_to_array(cmd, ' ');
  if (strcmp(shell->cmd->line, L"history") != 0
      && (shell->history->prev->data == NULL || strcmp(shell->cmd->line, shell->history->prev->data) != 0))
    push_before(shell->history, shell->cmd->line);
  log(shell->env, L"Calling command : ");
  log(shell->env, args[0]);
  log(shell->env, L"\n");
  while (shell->command_list[i])
    {
      if (strcmp(shell->command_list[i], args[0]) == 0)
	{
	  shell->cmd_ptr[i](shell, args);
	  break;
	}
      i++;
    }
  log(shell->env, L"command terminated with success\n");
  if (i == arraylen(shell->command_list))
    Print(L"Error: %s: command not found\r\n", args[0]);
  //show_array(args);
  free_array(args);
}
