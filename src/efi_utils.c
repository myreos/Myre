/*
** efi_utils.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Mon Feb 20 15:15:42 2017 grandr_r
** Last update Fri Apr 28 08:30:54 2017 grandr_r
*/

#include <Myre.h>

UINT8                   get_directory_last_occurency(CHAR16 *str)
{
  UINT8                 i;
  UINT8                 pos;

  pos = 0;
  i = 0;
  while (str[i])
    {
      if (str[i] == '/' || str[i] == '\\')
	pos = i;
      i++;
    }
  return (pos);
}

char 			is_directory(EFI_FILE *base_dir, CHAR16 *filename)
{
  EFI_STATUS		stat;
  EFI_FILE_HANDLE	tmp;
  EFI_FILE_INFO		*info;

  if (!base_dir)
    return (0);
  format_path(filename);
  stat = base_dir->Open(base_dir, &tmp, filename, EFI_FILE_MODE_READ, 0);
  if (EFI_ERROR(stat))
    return (0);
  info = LibFileInfo(tmp);
  if ((info->Attribute & EFI_FILE_DIRECTORY))
    return (1);
  FreePool(info);
  tmp->Close(tmp);
  return (0);
}

CHAR16			*get_full_path(CHAR16 *current_dir, CHAR16 *filename)
{
  CHAR16		*ret;

  ret = AllocatePool(sizeof(CHAR16 *) * (strlen(current_dir) + strlen(filename) + 10));
  strcpy(ret, current_dir);
  if (strcmp(current_dir, L"\\") != 0)
    strcat(ret, L"\\");
  strcat(ret, filename);
  return (ret);
}

char			file_exists(EFI_FILE *base_dir, CHAR16 *filename)
{
  EFI_STATUS		stat;
  EFI_FILE_HANDLE	tmp;

  if (!base_dir)
    return (0);
  format_path(filename);
  stat = base_dir->Open(base_dir, &tmp, filename, EFI_FILE_MODE_READ, 0);
  if (EFI_ERROR(stat))
    return (0);
  tmp->Close(tmp);
  return (1);
}

void	ResetKey(__attribute__((unused)) EFI_INPUT_KEY *key)
{
  key->ScanCode = 0;
  key->UnicodeChar = 0;
}
