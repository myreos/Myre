/*
** keyboard.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Tue Feb 21 08:06:52 2017 grandr_r
** Last update Fri Apr 28 09:03:32 2017 grandr_r
*/

#include <Myre.h>

void		idle(__attribute__((unused)) t_shell *shell)
{
  //if (shell->env->key.UnicodeChar != 0)
  //Print(L"%d\r\n", shell->env->key.UnicodeChar);
}

void		copy_array_match(CHAR16 **list, CHAR16 **ret, UINT8 *arr)
{
  UINT8		i;
  UINT8		j;

  i = 0;
  j = 0;
  while (list[i])
    {
      if (arr[i] == 1)
	ret[j++] = strdup(list[i]);
      i++;
    }
  ret[j] = NULL;
}

CHAR16		**array_matches(CHAR16 *str, CHAR16 **list, UINT8 *arr)
{
  UINT8		alloc_size;
  CHAR16	**ret;
  UINT8		len;
  UINT8		i;

  i = 0;
  alloc_size = 0;
  len = strlen(str);
  while (list[i])
    {
      arr[i] = 0;
      if (strncmp(list[i], str, len) == 0 && strcmp(list[i], L".") != 0 && strcmp(list[i], L"..") != 0)
	{
	  alloc_size += 1;
	  arr[i] = 1;
	}
      i++;
    }
  if (alloc_size == 0)
    return (NULL);
  ret = AllocatePool(sizeof(CHAR16 *) * (alloc_size + 1));
  copy_array_match(list, ret, arr);
  return (ret);
}

CHAR16		**tab_matches(t_shell *shell)
{
  UINT8		*arr;
  CHAR16	**ret;

  arr = AllocatePool(NB_CMD * sizeof(UINT8));
  ret = array_matches(shell->cmd->line, shell->command_list, arr);
  return (ret);
}

CHAR16			*get_full_dir_tab(t_shell *shell, CHAR16 *line, UINT8 *pos)
{
  CHAR16		*tmp;
  CHAR16		*ret;

  *pos = get_directory_last_occurency(line);
  if (*pos == 0)
    return (strdup(shell->current_dir));
  tmp = AllocatePool((*pos + 3) * sizeof(CHAR16));
  strncpy(tmp, line, *pos);
  ret = get_full_path(shell->current_dir, tmp);
  if (!is_directory(shell->env->base_dir, ret))
    {
      FreePool(ret);
      ret = NULL;
    }
  *pos = *pos + 1;
  FreePool(tmp);
  return (ret);
}

CHAR16			**file_matches(t_shell *shell)
{
  EFI_FILE_HANDLE       currfile;
  CHAR16		**list;
  CHAR16		**ret;
  CHAR16		**splitted_cmd;
  CHAR16		*full_dir;
  UINT8			*arr;
  EFI_STATUS		stat;
  UINT8			pos;

  //shell->env->base_dir->Open(shell->env->base_dir, &currfile, shell->current_dir, EFI_FILE_MODE_READ, 0);
  splitted_cmd = str_to_array(shell->cmd->line, ' ');
  full_dir = get_full_dir_tab(shell, splitted_cmd[arraylen(splitted_cmd) - 1], &pos);
  stat = shell->env->base_dir->Open(shell->env->base_dir, &currfile, full_dir, EFI_FILE_MODE_READ, 0);
  if (EFI_ERROR(stat))
    return (NULL);
  list = listing(currfile);
  arr = AllocatePool(arraylen(list) * sizeof(UINT8));
  //TODO SPLIT THIS LINE
  ret = array_matches(&splitted_cmd[arraylen(splitted_cmd) - 1][pos], list, arr);
  free_array(list);
  FreePool(arr);
  FreePool(full_dir);
  free_array(splitted_cmd);
  return (ret);
}

void		tab_key_command(t_shell *shell)
{
  CHAR16	**matches_list;

  matches_list = tab_matches(shell);
  if (matches_list == NULL)
    return;
  if (arraylen(matches_list) == 1)
    {
      strcpy(shell->cmd->line, matches_list[0]);
      shell->cmd->pos = strlen(shell->cmd->line);
      shell->cmd->command_finished = 1;
    }
  else
    {
      Print(L"\r\n");
      show_array(matches_list);
    }
  Print(L"\r%es%s", PROMPT, shell->cmd->line);
  free_array(matches_list);
}

void		tab_key_file(t_shell *shell)
{
  CHAR16	**matches_list;
  CHAR16	**arr;
  UINT8		pos;

  matches_list = file_matches(shell);
  if (matches_list == NULL)
    return;
  if (arraylen(matches_list) == 1)
    {
      arr = str_to_array(shell->cmd->line, ' ');
      //strcpy(shell->cmd->line + (shell->cmd->pos - strlen(arr[arraylen(arr) - 1])), matches_list[0]);
      pos = get_directory_last_occurency(arr[arraylen(arr) - 1]);
      if (pos != 0)
	pos++;
      strcpy(shell->cmd->line + (shell->cmd->pos - strlen(arr[arraylen(arr) - 1]) + pos), matches_list[0]);
      free_array(arr);
      shell->cmd->pos = strlen(shell->cmd->line);
    }
  else
    {
      Print(L"\r\n");
      show_array(matches_list);
    }
  Print(L"\r%es%s", PROMPT, shell->cmd->line);
  free_array(matches_list);
}

void		tab_key(t_shell *shell)
{
  if (shell->cmd->command_finished == 1)
    tab_key_file(shell);
  else
    tab_key_command(shell);
}

void		print_key(t_shell *shell)
{
  insert_char(shell, shell->env->key.UnicodeChar);
  shell->cmd->pos++;
  if (shell->cmd->pos + strlen(PROMPT) < 80)
    print_line(shell);
  else
    Print(L"%s", shell->cmd->line + (shell->cmd->pos - 1));
  if (shell->env->key.UnicodeChar == ' ')
    shell->cmd->command_finished = 1;
}

void		delete_key(t_shell *shell)
{
  if (shell->cmd->pos == strlen(shell->cmd->line))
    return;
  erase_line(shell);
  remove_char(shell, shell->cmd->pos);
  print_line(shell);
}

void		enter_key(t_shell *shell)
{
  while (shell->history->next != NULL)
    shell->history = shell->history->next;
  Print(L"\r\n");
  interpret_command(shell, shell->cmd->line);
  Print(L"%es", PROMPT);
  memset(shell->cmd->line, 0, CMD_LEN);
  shell->cmd->pos = 0;
  shell->cmd->command_finished = 0;
}

void		escape_key(t_shell *shell)
{
  shell->loop = 0;
}

void		backspace_key(t_shell *shell)
{
  if (shell->cmd->pos == 0)
    return;
  erase_line(shell);
  remove_char(shell, shell->cmd->pos - 1);
  shell->cmd->pos--;
  print_line(shell);
}

void		key_up(__attribute__((unused)) t_shell *shell)
{
  UINT8		i;
  //Print(L"UP");
  if (shell->history->prev->data == NULL)
    return;
  i = strlen(PROMPT) + shell->cmd->pos;
  Print(L"\r");
  while (i--)
    Print(L" ");
  shell->history = shell->history->prev;
  memset(shell->cmd->line, 0, CMD_LEN);
  strcpy(shell->cmd->line, shell->history->data);
  shell->cmd->pos = strlen(shell->cmd->line);
  Print(L"\r%es", PROMPT);
  Print(shell->cmd->line);
}

void		key_down(__attribute__((unused)) t_shell *shell)
{
  UINT8		i;
  //Print(L"DOWN");
  if (shell->history->next->data == NULL)
    return;
  i = strlen(PROMPT) + shell->cmd->pos;
  Print(L"\r");
  while (i--)
    Print(L" ");
  shell->history = shell->history->next;
  memset(shell->cmd->line, 0, CMD_LEN);
  strcpy(shell->cmd->line, shell->history->data);
  shell->cmd->pos = strlen(shell->cmd->line);
  Print(L"\r%es", PROMPT);
  Print(shell->cmd->line);
}

void		key_right(__attribute__((unused)) t_shell *shell)
{
  if (shell->cmd->pos == strlen(shell->cmd->line))
    return;
  ST->ConOut->SetCursorPosition(ST->ConOut, ST->ConOut->Mode->CursorColumn + 1, ST->ConOut->Mode->CursorRow);
  shell->cmd->pos++;
}

void		key_left(__attribute__((unused)) t_shell *shell)
{
  if (shell->cmd->pos == 0)
    return;
  ST->ConOut->SetCursorPosition(ST->ConOut, ST->ConOut->Mode->CursorColumn - 1, ST->ConOut->Mode->CursorRow);
  shell->cmd->pos--;
}

void		init_keyboard_ptr(void (**char_func)(t_shell *shell), void (**key_func)(t_shell *shell))
{
  int		i;

  i = 0;
  /* initializing ptr on function for char key (a, b, c, ...) */
  while (i < 127)
    {
      char_func[i] = print_key;
      i++;
    }
  char_func[0] = idle;
  char_func[4] = escape_key;
  char_func[8] = backspace_key;
  char_func[9] = tab_key;
  char_func[13] = enter_key;

  /* inializing ptr on function for other key (left, right, ...) */
  i = 0;
  while (i < 25)
    {
      key_func[i] = idle;
      i++;
    }
  key_func[1] = key_up;
  key_func[2] = key_down;
  key_func[3] = key_right;
  key_func[4] = key_left;
  key_func[8] = delete_key;
  key_func[23] = escape_key;
}
