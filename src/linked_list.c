/*
** linked_list.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Tue Apr 18 08:01:29 2017 grandr_r
** Last update Wed Apr 19 13:47:46 2017 grandr_r
*/

#include <Myre.h>

t_link          *init_link(CHAR16 *data)
{
  t_link        *ret;

  if ((ret = AllocatePool(sizeof(*ret))) == NULL)
    return (NULL);
  ret->data = strdup(data);
  ret->next = NULL;
  ret->prev = NULL;
  return (ret);
}

void            push_tail(t_link *list, CHAR16 *elem)
{
  t_link        *tmp;

  tmp = list;
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->next = AllocatePool(sizeof(*tmp->next));
  if (tmp->next == NULL)
    return;
  tmp->next->next = NULL;
  tmp->next->data = strdup(elem);
  tmp->next->prev = tmp;
}

void		push_after(t_link *list, CHAR16 *elem)
{
  t_link        *tmp;

   if ((tmp = AllocatePool(sizeof(*tmp))) == NULL)
    return;
   tmp->data = strdup(elem);
   tmp->prev = list;
   tmp->next = list->next;
   list->next->prev = tmp;
   list->next = tmp;
}

void		push_head(t_link *list, CHAR16 *elem)
{
  t_link        *tmp;

  tmp = list;
  while (tmp->prev != NULL)
    tmp = tmp->prev;
  if ((tmp->prev = AllocatePool(sizeof(*tmp))) == NULL)
    return;
  tmp->prev->next = tmp;
  tmp = tmp->prev;
  tmp->data = strdup(elem);
  tmp->prev = NULL;
}

void		push_before(t_link *list, CHAR16 *elem)
{
  t_link        *tmp;

   if ((tmp = AllocatePool(sizeof(*tmp))) == NULL)
    return;
   tmp->data = strdup(elem);
   tmp->prev = list->prev;
   tmp->next = list;
   list->prev->next = tmp;
   list->prev = tmp;
}

void            show_link(t_link *list)
{
  t_link        *tmp;

  tmp = list;
  while (tmp->prev != NULL)
    tmp = tmp->prev;
  while (tmp->next != NULL)
    {
      Print(L"%s\n", tmp->data);
      tmp = tmp->next;
    }
}

void            free_link(t_link *root)
{
  t_link        *tmp;
  t_link        *node;

  tmp = root;
  while (tmp != NULL)
    {
      node = tmp;
      FreePool(tmp->data);
      tmp = tmp->next;
      FreePool(node);
    }
}
