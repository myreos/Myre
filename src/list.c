/*
** list.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Mon Apr  3 11:19:37 2017 grandr_r
** Last update Tue Jul 25 14:19:28 2017 grandr_r
*/

#include <Myre.h>

void		free_list(t_list *list)
{
  FreePool(list);
}

void		display_list(t_list *list)
{
  UINT8		i;
  char 		display_path;

  display_path = strcmp(getenv_cmd(list->env, L"DISPLAY_PATH="), L"true");
  ST->ConOut->ClearScreen(ST->ConOut);
  ST->ConOut->EnableCursor(ST->ConOut, FALSE);
  ST->ConOut->SetCursorPosition(ST->ConOut, 0, 0);
  Print(MYRE_TEXT);
  while (i < list->nb_entry)
    {
      if (i == list->index)
	Print(L"* ");
      else
	Print(L"  ");
      Print(L"%s", get_entry_name(list->env, i));
      if (display_path == 0)
	Print(L" (from %s)", get_entry_path(list->env, i));
      Print(L"\r\n");
      i++;
    }
}

void                    list_boot(t_list *list)
{
  CHAR16                *filename;
  EFI_DEVICE_PATH       *path;
  EFI_HANDLE            image;
  EFI_STATUS            err;

  filename = getenv_cmd(list->env, L"SELECTED_EFI=");
  path = FileDevicePath(list->env->loaded_image->DeviceHandle, filename);
  if (!path)
    return;
  err = uefi_call_wrapper(BS->LoadImage, 6, FALSE, list->env->base_image, path, NULL, 0, &image);
  if (EFI_ERROR(err))
    {
      Print(L"\r\n\r\nError loading %s: %r", filename, err);
      return;
    }
  Print(L"Booting %s...\r\n", filename);
  free_env(list->env);
  err = uefi_call_wrapper(BS->StartImage, 3, image, NULL, NULL);
  if (EFI_ERROR(err))
    {
      Print(L"Error while booting %s : %r\r\n", filename, err);
      return;
    }
  list->loop = 0;
}

void		index_inc(t_list *list)
{
  CHAR16	*entry;

  if (list->index + 1 > list->nb_entry - 1)
    return;
  list->index++;
  entry = get_entry_path(list->env, list->index);
  entry_to_env(list->env, entry);
  FreePool(entry);
  display_list(list);
}

void		index_dec(t_list *list)
{
  CHAR16	*entry;

  if (list->index - 1 < 0)
    return;
  list->index--;
  entry = get_entry_path(list->env, list->index);
  entry_to_env(list->env, entry);
  FreePool(entry);
  display_list(list);
}

void		list_shutdown(t_list *list)
{
  free_env(list->env);
  free_list(list);
  RT->ResetSystem(EfiResetShutdown, EFI_SUCCESS, 0, NULL);
}

t_list		*init_list(t_env *env)
{
  t_list	*ret;
  CHAR16	*tmp;

  ret = AllocatePool(sizeof(*ret));
  ret->index = atoi(getenv_cmd(env, L"DEFAULT_ENTRY=")) - 1;
  ret->loop = 1;
  ret->env = env;
  ret->nb_entry = get_nb_entry(env);
  tmp = get_entry_path(ret->env, ret->index);
  entry_to_env(ret->env, tmp);
  ret->ptr_func[0] = index_inc;
  ret->ptr_func[1] = index_dec;
  ret->ptr_func[2] = list_boot;
  ret->ptr_func[3] = list_shutdown;
  ret->ptr_func[4] = NULL;
  FreePool(tmp);
  return (ret);
}

void		list_call_wrapper(t_list *list)
{
  UINT8		index;
  UINT8		arr[NB_CMD_LIST] = {2, 1, 13, 23};
  UINT8		i;

  index = list->env->key.UnicodeChar + list->env->key.ScanCode;
  i = 0;
  while (i < NB_CMD_LIST)
    {
      if (index == arr[i])
	list->ptr_func[i](list);
      i++;
    }
}

EFI_STATUS	list_loop(t_env *env)
{
  t_list	*list;

  list = init_list(env);
  display_list(list);
  while (list->loop)
    {
      ST->ConIn->ReadKeyStroke(ST->ConIn, &list->env->key);
      list_call_wrapper(list);
      ResetKey(&list->env->key);
    }
  return (EFI_SUCCESS);
}
