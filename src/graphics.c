/*
** graphics.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Tue Mar 21 08:42:39 2017 grandr_r
** Last update Tue Jul 25 14:18:04 2017 grandr_r
*/

#include <Myre.h>
#include <picopng.h>

void				clear_screen(t_graphics *screen)
{
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL pix;
  UINTN				i;

  i = 0;
  pix.Blue = 0;
  pix.Green = 0;
  pix.Red = 0;
  pix.Reserved = 0;
  while (i < screen->height * screen->width)
    {
      screen->pixels[i] = pix;
      i++;
    }
  //screen->screen->Blt(screen->screen, screen->pixels, EfiBltBufferToVideo, 0, 0, 0, 0, screen->width, screen->height, 0);
}


void			disable_error_animation(t_graphics *screen)
{
  t_img			*err_img;
  UINTN			i;
  INTN			count;

  count = 255;
  while (count > 100)
    {
      err_img = init_image(screen->height, screen->width);
      i = 0;
      while (i < screen->height * screen->width)
	{
	  if ((INTN)count - screen->pixels[i].Red > 0)
	    err_img->data[i].Red = count - screen->pixels[i].Red;
	  else
	    err_img->data[i].Red = screen->pixels[i].Red;
	  err_img->data[i].Blue = screen->pixels[i].Blue;
	  err_img->data[i].Green = screen->pixels[i].Green;
	  i++;
	}
      screen->screen->Blt(screen->screen, err_img->data, EfiBltBufferToVideo, 0, 0, 0, 0, screen->width, screen->height, 0);
      free_image(err_img);
      count -= 33;
    }
}

void			error_animation(t_graphics *screen)
{
  t_img			*err_img;
  UINTN			i;

  i = 0;
  err_img = init_image(screen->height, screen->width);
  while (i < screen->height * screen->width)
    {
      err_img->data[i].Red = 255 - screen->pixels[i].Red;
      err_img->data[i].Blue = screen->pixels[i].Blue;
      err_img->data[i].Green = screen->pixels[i].Green;
      i++;
    }
  screen->screen->Blt(screen->screen, err_img->data, EfiBltBufferToVideo, 0, 0, 0, 0, screen->width, screen->height, 0);
  disable_error_animation(screen);
  screen->screen->Blt(screen->screen, screen->pixels, EfiBltBufferToVideo, 0, 0, 0, 0, screen->width, screen->height, 0);
  free_image(err_img);
}

void				boot_animation(t_graphics *screen)
{
  UINTN				i;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL pix = {0, 0, 0, 0};

  i = 0;
  while (i < screen->width)
    {
      //clear_screen(screen);
      //draw_image_at(i, 0, screen, screen->background);
      screen->screen->Blt(screen->screen, &pix, EfiBltVideoFill, 0, 0, 0, 0, i, screen->height, 0);
      //screen->screen->Blt(screen->screen, screen->background->data, EfiBltBufferToVideo, 0, 0, i, 0, screen->width - i, screen->height, screen->width * sizeof(EFI_GRAPHICS_OUTPUT_BLT_PIXEL));
      //Print(L"here");
      //BS->Stall(100000);
      //i += screen->width / 7;
      i += screen->width / 5;
    }
  screen->screen->Blt(screen->screen, &pix, EfiBltVideoFill, 0, 0, 0, 0, screen->width, screen->height, 0);
}

void			graphics_boot(t_graphics *screen)
{
  CHAR16                *filename;
  EFI_DEVICE_PATH       *path;
  EFI_HANDLE		image;
  EFI_STATUS		err;

  filename = getenv_cmd(screen->env, L"SELECTED_EFI=");
  path = FileDevicePath(screen->env->loaded_image->DeviceHandle, filename);
  log(screen->env, L"Booting : ");
  log(screen->env, filename);
  log(screen->env, L" : ");
  if (!path)
    return;
  err = uefi_call_wrapper(BS->LoadImage, 6, FALSE, screen->env->base_image, path, NULL, 0, &image);
  if (EFI_ERROR(err))
    {
      log(screen->env, L"fail\n");
      // launch here an animation of error
      error_animation(screen);
      return;
    }
  // launch an animation of end here !
  //clear_screen(screen);
  log(screen->env, L"done\n");
  boot_animation(screen);
  free_env(screen->env);
  err = uefi_call_wrapper(BS->StartImage, 3, image, NULL, NULL);
  if (EFI_ERROR(err))
    {
      error_animation(screen);
      // launch here the same animation
      return;
    }
  screen->loop = 0;
}

void				read_icons(t_graphics *screen)
{
  CHAR16	*filename;
  UINT8		i;
  UINTN		size;

  i = 0;
  size = screen->width / screen->nb_entry / 3;
  while (i < screen->nb_entry)
    {
      screen->icons[i] = NULL;
      i++;
    }
  i = 0;
  while (i < screen->nb_entry)
    {
      filename = get_entry_icon(screen->env, i);
      screen->icons[i] = read_from_png(screen->env, format_path(filename));
      screen->icons[i] = resize_image(screen->icons[i], size, size);
      //add here an animation of icon appear
      //screen->screen->Blt(screen->screen, screen->pixels, EfiBltBufferToVideo, 0, 0, 0, 0, screen->width, screen->height, 0);
      FreePool(filename);
      i++;
    }
}

void				free_graphics(t_graphics *screen)
{
  UINTN				i;

  i = 0;
  log(screen->env, L"Releasing graphics ressources : ");
  while (i < screen->nb_entry)
    {
      free_image(screen->icons[i]);
      free_image(screen->icons_drawn[i]);
      free_image(screen->icons_drawn_up[i]);
      free_image(screen->icons_drawn_mid[i]);
      i++;
    }
  //FreePool(screen->icons);
  //FreePool(screen->icons_drawn);
  //FreePool(screen->icons_drawn_up);
  //FreePool(screen->icons_drawn_mid);
  FreePool(screen->pixels);
  free_image(screen->background);
  log(screen->env, L" : done\n");
  FreePool(screen);
}

void				redraw_back(t_graphics *screen)
{
  draw_image_at(0, 0, screen, screen->background);
}

void				index_inc_graphics(t_graphics *screen)
{
  if (screen->index + 1 > screen->nb_entry - 1)
    return;
  screen->index++;
  moving_animation(screen);
}

void				index_dec_graphics(t_graphics *screen)
{
  if (screen->index - 1 < 0)
    return;
  screen->index--;
  moving_animation(screen);
}

void				switch_shell(t_graphics *screen)
{
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL pix = {0, 0, 0, 0};

  screen->screen->Blt(screen->screen, &pix, EfiBltVideoFill, 0, 0, 0, 0, screen->width, screen->height, 0);
  ResetKey(&screen->env->key);
  shell_loop(screen->env);
  screen->screen->Blt(screen->screen, screen->pixels, EfiBltBufferToVideo, 0, 0, 0, 0, screen->width, screen->height, 0);
}

void				shutdown_graphics(__attribute__((unused)) t_graphics *screen)
{
  //free_env(screen->env);
  RT->ResetSystem(EfiResetShutdown, EFI_SUCCESS, 0, NULL);
}

t_graphics			*init_graphics(t_env *env)
{
  EFI_STATUS 			stat;
  t_graphics			*ret;
  EFI_GUID			EfiGraphicsProtocol = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;

  log(env, L"\nAllocating ressources : ");
  ret = AllocatePool(sizeof(*ret));

  ret->icons_drawn = AllocatePool(sizeof(t_img *) * get_nb_entry(env));
  ret->icons_drawn_up = AllocatePool(sizeof(t_img *) * get_nb_entry(env));
  ret->icons_drawn_mid = AllocatePool(sizeof(t_img *) * get_nb_entry(env));

  stat = LibLocateProtocol(&EfiGraphicsProtocol, (void **)&ret->screen);
  if (EFI_ERROR(stat))
    {
      FreePool(ret);
      return (NULL);
    }
  ret->width = ret->screen->Mode->Info->HorizontalResolution;
  ret->height = ret->screen->Mode->Info->VerticalResolution;
  ret->env = env;
  //ret->pixels = AllocatePool(ret->height * ret->width * sizeof(EFI_GRAPHICS_OUTPUT_BLT_PIXEL));
  log(env, L" Done\n");
  ret->loop = 1;
  log(env, L"Computing entries number : ");
  ret->index = atoi(getenv_cmd(env, L"DEFAULT_ENTRY=")) - 1;
  ret->nb_entry = get_nb_entry(env);
  log(env, L" Done\n");
  log(env, L"Reading wallpaper : ");
  ret->background = read_from_png(env, format_path(getenv_cmd(env, L"BACKGROUND_IMAGE=")));
  ret->background = resize_image(ret->background, ret->height, ret->width);
  //TODO : CREATE T_IMG * AND FREE IT INSTEAD OF THIS
  ret->pixels = copy_img(ret->background)->data;
  log(env, L" Done\n");
  //ret->screen->Blt(ret->screen, ret->pixels, EfiBltBufferToVideo, 0, 0, 0, 0, ret->width, ret->height, 0);
  log(env, L"Reading icons : ");
  ret->icons = AllocatePool(sizeof(t_img *) * ret->nb_entry);
  ret->ptr_func[0] = index_dec_graphics;
  ret->ptr_func[1] = index_inc_graphics;
  ret->ptr_func[2] = graphics_boot;
  ret->ptr_func[3] = switch_shell;
  ret->ptr_func[4] = shutdown_graphics;
  ret->ptr_func[5] = NULL;
  //redraw_back(ret);
  read_icons(ret);
  log(env, L" Done\n");
  return (ret);
}

void		redraw_back_zone(t_graphics *screen, t_img *img, UINTN margin, UINTN curr_height)
{
  UINTN		startx;
  UINTN		endx;
  UINTN		starty;
  UINTN		endy;

  starty = (screen->height / 2) - (img->width / 2) - (curr_height * 2) / 2;
  endy = (screen->height / 2) - (img->width / 2) + (curr_height * 2) / 2 + img->height;
  endx = margin + img->width;
  while (starty < endy)
    {
      startx = margin;
      while (startx < endx)
	{
	  screen->pixels[screen->width * starty + startx].Red = screen->background->data[screen->width * starty + startx].Red;
	  screen->pixels[screen->width * starty + startx].Blue = screen->background->data[screen->width * starty + startx].Blue;
	  screen->pixels[screen->width * starty + startx].Green = screen->background->data[screen->width * starty + startx].Green;
	  startx++;
	}
      starty++;
    }
}

void		display_zone(t_graphics *screen, t_img *img, UINTN margin, UINTN curr_height)
{
  UINTN		startx;
  UINTN		endx;
  UINTN		starty;
  UINTN		endy;

  starty = (screen->height / 2) - (img->width / 2) - (curr_height * 2) / 2;
  endy = (screen->height / 2) - (img->width / 2) + (curr_height * 2) / 2 + img->height;
  startx = margin;
  endx = margin + img->width;
  //(void)endx;
  //(void)endy;
  (void)startx;
  (void)starty;
  (void)endx;
  (void)endy;
  //(void)starty;
  //screen->screen->Blt(screen->screen, screen->pixels, EfiBltBufferToVideo, startx, starty, startx, starty, (endx - startx), (endy - starty), (screen->width) * sizeof(EFI_GRAPHICS_OUTPUT_BLT_PIXEL));
  screen->screen->Blt(screen->screen, screen->pixels, EfiBltBufferToVideo, 0, starty, 0, starty, screen->width, endy, 0);
  //screen->screen->Blt(screen->screen, img->data, EfiBltBufferToVideo, 0, 0, startx, starty, img->width, img->height, 0);
  (void)img;
  (void)curr_height;
  (void)screen;
}

/*void		moving_animation(t_graphics *screen)
{
  UINTN		i;
  UINTN		margin;
  UINTN		animation_height;
  UINTN		curr_height;
  CHAR16	*file;
  UINTN		max_loop;

  animation_height = screen->height / 40;
  curr_height = 0;
  max_loop = screen->nb_entry;
  //TODO : IF STILL TOO SLOW FOR 4K : just loop from index - 1 to index + 1 (avoid looping untouched icons) AND try to redraw only zone not Blt all screen
  while (curr_height < animation_height)
    {
      i = screen->index - 1 < 0 ? 0 : screen->index - 1;
      while (i < max_loop)
	{
	  margin = (screen->width / screen->nb_entry) * i + screen->icons[i]->width;
	  redraw_back_zone(screen, screen->icons[i], margin, animation_height);
	  if (i == screen->index)
	    draw_image_at(margin, (screen->height / 2) - (screen->icons[i]->width / 2) - (curr_height * 2) / 2, screen, screen->icons[i]);
	  else
	    draw_image_at(margin, screen->height / 2 - screen->icons[i]->width / 2, screen, screen->icons[i]);
	  i++;
	}
      curr_height += animation_height / 5;
      //curr_height += animation_height;
      screen->screen->Blt(screen->screen, screen->pixels, EfiBltBufferToVideo, 0, 0, 0, 0, screen->width, screen->height, 0);
      //TODO REPLACE THIS BY REDISPLAY ZONE (REPLACE 0 BY BUFFER WIDTH)
    }
  file = get_entry_path(screen->env, screen->index);
  entry_to_env(screen->env, file);
  FreePool(file);
}*/

void		moving_animation(t_graphics *screen)
{
  UINTN		i;
  CHAR16	*file;
  UINTN		max_loop;
  UINTN		margin;
  UINTN		height;

  max_loop = screen->nb_entry;
  i = screen->index - 1 < 0 ? 0 : screen->index - 1;
  while (i < max_loop)
    {
      margin = (screen->width / screen->nb_entry) * i + screen->icons[i]->width;
      height = screen->height / 2 - screen->icons[i]->width / 2 - (screen->height / 20);
      if (i == screen->index)
	screen->screen->Blt(screen->screen, screen->icons_drawn_up[i]->data, EfiBltBufferToVideo, 0, 0, margin, height, screen->icons_drawn_up[i]->width, screen->icons_drawn_up[i]->height, 0);
      else
	screen->screen->Blt(screen->screen, screen->icons_drawn[i]->data, EfiBltBufferToVideo, 0, 0, margin, height, screen->icons_drawn[i]->width, screen->icons_drawn[i]->height, 0);
      i++;
    }
  file = get_entry_path(screen->env, screen->index);
  entry_to_env(screen->env, file);
  FreePool(file);
}

void		draw_icons_up(t_graphics *screen)
{
  UINTN		i;
  UINTN		margin;

  i = 0;
  while (i < screen->nb_entry)
    {
      margin = (screen->width / screen->nb_entry) * i + screen->icons[i]->width;
      //redraw_back_zone(screen, screen->icons[i], margin, animation_height);
      draw_image_at(margin, (screen->height / 2) - (screen->icons[i]->height / 2) - (screen->height / 20), screen, screen->icons[i]);
      i++;
    }
}

void		draw_icons_mid(t_graphics *screen)
{
  UINTN		i;
  UINTN		margin;

  i = 0;
  while (i < screen->nb_entry)
    {
      margin = (screen->width / screen->nb_entry) * i + screen->icons[i]->width;
      //redraw_back_zone(screen, screen->icons[i], margin, animation_height);
      draw_image_at(margin, (screen->height / 2) - (screen->icons[i]->height / 2) - (screen->height / 20), screen, screen->icons[i]);
      i++;
    }
}

void		draw_icons_down(t_graphics *screen)
{
  UINTN		i;
  UINTN		margin;

  i = 0;
  while (i < screen->nb_entry)
    {
      margin = (screen->width / screen->nb_entry) * i + screen->icons[i]->width;
      //redraw_back_zone(screen, screen->icons[i], margin, animation_height);
      draw_image_at(margin, screen->height / 2 - screen->icons[i]->width / 2, screen, screen->icons[i]);
      i++;
    }
}

void		init_icons_state_mid(t_graphics *screen)
{
  UINTN		i;
  UINTN		margin;
  UINTN		height;

  i = 0;
  height = screen->height / 2 - screen->icons[i]->width / 2 - (screen->height / 20);
  redraw_back(screen);
  draw_icons_mid(screen);
  while (i < screen->nb_entry)
    {
      //TODO : REMOVE THIS AND REPLACE BY DRAW ICON AT AND THEN COPY THIS
      // (HAVE TO DRAW ALL ICON UP, COPY THEN ALL ICON DOWN AND COPY
      margin = (screen->width / screen->nb_entry) * i + screen->icons[i]->width;
      screen->icons_drawn_mid[i] = copy_pixels_zone(margin, height, screen->icons[i]->width, screen->icons[i]->height + screen->icons[i]->height / 2, screen);
      i++;
    }
}

void		init_icons_state_up(t_graphics *screen)
{
  UINTN		i;
  UINTN		margin;
  UINTN		height;

  i = 0;
  height = screen->height / 2 - screen->icons[i]->width / 2 - (screen->height / 20);
  redraw_back(screen);
  draw_icons_up(screen);
  while (i < screen->nb_entry)
    {
      //TODO : REMOVE THIS AND REPLACE BY DRAW ICON AT AND THEN COPY THIS
      // (HAVE TO DRAW ALL ICON UP, COPY THEN ALL ICON DOWN AND COPY
      margin = (screen->width / screen->nb_entry) * i + screen->icons[i]->width;
      screen->icons_drawn_up[i] = copy_pixels_zone(margin, height, screen->icons[i]->width, screen->icons[i]->height + screen->icons[i]->height / 2, screen);
      i++;
    }
}

//TODO FUSE THIS TWO AND PASS A PARAMTER CALLED ICON_SET (UP OR DOWN)

void		init_icons_state_down(t_graphics *screen)
{
  UINTN		i;
  UINTN		margin;
  UINTN		height;

  i = 0;
  height = screen->height / 2 - screen->icons[i]->width / 2 - (screen->height / 20);
  redraw_back(screen);
  draw_icons_down(screen);
  while (i < screen->nb_entry)
    {
      //TODO : REMOVE THIS AND REPLACE BY DRAW ICON AT AND THEN COPY THIS
      // (HAVE TO DRAW ALL ICON UP, COPY THEN ALL ICON DOWN AND COPY
      margin = (screen->width / screen->nb_entry) * i + screen->icons[i]->width;
      screen->icons_drawn[i] = copy_pixels_zone(margin, height, screen->icons[i]->width, screen->icons[i]->height + screen->icons[i]->height / 2, screen);
      i++;
    }
}


void			graphics_call_wrapper(t_graphics *screen)
{
  UINT8         index;
  UINT8         arr[NB_CMD_GRAPHICS] = {4, 3, 13, 115, 23};
  UINT8         i;

  index = screen->env->key.UnicodeChar + screen->env->key.ScanCode;
  i = 0;
  while (i < NB_CMD_GRAPHICS)
    {
      if (index == arr[i])
	screen->ptr_func[i](screen);
      i++;
    }
}

EFI_STATUS      graphics_loop(t_env *env)
{
  t_graphics	*screen;

  screen = init_graphics(env);
  screen->screen->Blt(screen->screen, screen->pixels, EfiBltBufferToVideo, 0, 0, 0, 0, screen->width, screen->height, 0);
  init_icons_state_up(screen);
  init_icons_state_down(screen);
  moving_animation(screen);
  while (screen->loop)
    {
      ST->ConIn->ReadKeyStroke(ST->ConIn, &screen->env->key);
      if (screen->env->key.UnicodeChar != 0 || screen->env->key.ScanCode != 0)
	graphics_call_wrapper(screen);
      ResetKey(&screen->env->key);
    }
  free_graphics(screen);
  return (EFI_SUCCESS);
}

//BLT : STARTX = img start x
//	STARTY = img start y
//	DESTINATIONX = SCREEN START X
//	DESTINATIONY = SCREEN START Y
//

//screen->screen->Blt(screen->screen, img->data, EfiBltBufferToVideo, 0, 0, 0, 0, img->height, img->width, 0);
