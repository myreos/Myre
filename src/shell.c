/*
** shell.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Mon Feb 20 15:14:17 2017 grandr_r
** Last update Wed May  3 08:17:49 2017 grandr_r
*/

#include <Myre.h>

t_cmd		*init_cmd()
{
  t_cmd		*cmd;

  cmd = AllocatePool(sizeof(*cmd));
  memset(cmd->line, 0, CMD_LEN);
  cmd->pos = 0;
  return (cmd);
}

void		init_command_index(t_shell *shell)
{
  shell->command_list[0] = L"exit";
  shell->command_list[1] = L"echo";
  shell->command_list[2] = L"shutdown";
  shell->command_list[3] = L"reboot";
  shell->command_list[4] = L"ls";
  shell->command_list[5] = L"boot";
  shell->command_list[6] = L"env";
  shell->command_list[7] = L"getenv";
  shell->command_list[8] = L"setenv";
  shell->command_list[9] = L"unsetenv";
  shell->command_list[10] = L"clear";
  shell->command_list[11] = L"select";
  shell->command_list[12] = L"help";
  shell->command_list[13] = L"list";
  shell->command_list[14] = L"graphics-start";
  shell->command_list[15] = L"cd";
  shell->command_list[16] = L"pwd";
  shell->command_list[17] = L"history";
  shell->command_list[18] = L"cat";
  shell->command_list[19] = L"update-myre";
  shell->command_list[NB_CMD - 1] = NULL;
}

void		init_command_ptr(t_shell *shell)
{
  shell->cmd_ptr[0] = exit;
  shell->cmd_ptr[1] = echo;
  shell->cmd_ptr[2] = shutdown;
  shell->cmd_ptr[3] = reboot;
  shell->cmd_ptr[4] = ls;
  shell->cmd_ptr[5] = boot;
  shell->cmd_ptr[6] = env;
  shell->cmd_ptr[7] = getenv;
  shell->cmd_ptr[8] = setenv;
  shell->cmd_ptr[9] = unsetenv;
  shell->cmd_ptr[10] = clear;
  shell->cmd_ptr[11] = select;
  shell->cmd_ptr[12] = help;
  shell->cmd_ptr[13] = list;
  shell->cmd_ptr[14] = graphics_start;
  shell->cmd_ptr[15] = cd;
  shell->cmd_ptr[16] = pwd;
  shell->cmd_ptr[17] = history;
  shell->cmd_ptr[18] = cat;
  shell->cmd_ptr[19] = update_myre;
}

t_shell		*init_shell(t_env *env)
{
  t_shell	*ret;

  log(env, L"\nAllocating shell ressources :");
  ret = AllocatePool(sizeof(*ret));
  ret->loop = 1;
  ret->cmd = init_cmd();
  init_keyboard_ptr(ret->char_func, ret->key_func);
  init_command_index(ret);
  init_command_ptr(ret);
  ret->env = env;
  if (getenv_cmd(ret->env, L"CONFIG="))
    Print(L"Error: %s: file not found. Using default config...\r\n", L"\\EFI\\Myre\\Myre.cfg");
  if (getenv_cmd(ret->env, L"CONFIG_ERROR"))
      Print(L"Error: Config not valid: Please check Myre.log to have more details about this error\r\n");
  ret->current_dir = strdup(getenv_cmd(ret->env, L"DEFAULT_DIR="));
  ret->history = init_link(L"");
  log(env, L" done\n");
  return (ret);
}

void		free_shell(t_shell *shell)
{
  log(shell->env, L"Releasing shell ressouces : ");
  FreePool(shell->cmd);
  free_link(shell->history);
  FreePool(shell->current_dir);
  //free_env(shell->env);
  log(shell->env, L" done\n");
  FreePool(shell);
}

void		init_console()
{
  ST->ConOut->ClearScreen(ST->ConOut);
  ST->ConOut->EnableCursor(ST->ConOut, TRUE);
  //ST->ConOut->SetCursorPosition(ST->ConOut, 0, 0);
}

EFI_STATUS	shell_loop(t_env *env)
{
  EFI_STATUS    err;
  t_shell	*shell;

  init_console();
  Print(GREETINGS);
  if (getenv_cmd(env, L"ERROR_MODE="))
    Print(L"Error : %s : No such mode. Default mode selected\r\n", getenv_cmd(env, L"MODE="));
  shell = init_shell(env);
  Print(L"%es", PROMPT);
  while (shell->loop)
    {
      err = ST->ConIn->ReadKeyStroke(ST->ConIn, &shell->env->key);
      shell->key_func[shell->env->key.ScanCode](shell);
      shell->char_func[shell->env->key.UnicodeChar](shell);
      ResetKey(&shell->env->key);
    }
  //free_shell(shell);
  return (err);
}
