/*
** read_config.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Wed Mar  1 07:50:44 2017 grandr_r
** Last update Wed May  3 08:58:43 2017 grandr_r
*/

#include <Myre.h>

void			verify_file(t_env *env, CHAR16 *filename, char *config_valid)
{
  char 			result;
  log(env, L"Verifying file: \"");
  log(env, filename);
  result = file_exists(env->base_dir, filename);
  if (result == 0)
    {
      log(env, L"\" : fail\n");
      *config_valid = 0;
    }
  else
    log(env, L"\" : done\n");
}

void			verify_default_entry(t_env *env)
{
  int			res;
  int			nb_entries;
  CHAR16		**entries;
  CHAR16		*entries_list;

  log(env, L"Verifying default entry...\n");
  res = atoi(getenv_cmd(env, L"DEFAULT_ENTRY="));
  entries_list = strdup(getenv_cmd(env, L"ENTRY_LIST"));
  entries = str_to_array(entries_list, '|');
  nb_entries = arraylen(entries);
  if (res <= 0 || res > nb_entries)
    {
      log(env, L"Default entry not in range, defaulting to 1\n");
      add_to_env(env, L"DEFAULT_ENTRY=1");
    }
  FreePool(entries_list);
  free_array(entries);
}

void			verify_images_list(t_env *env, char *config_valid, UINT8 *nb_icon)
{
  CHAR16		*list;
  CHAR16		**splitted_list;
  UINT8			i;

  i = 0;
  log(env, L"Verifying BACKGROUND_IMAGE...\n");
  verify_file(env, getenv_cmd(env, L"BACKGROUND_IMAGE="), config_valid);
  log(env, L"Verifying ICON_LIST...\n");
  list = strdup(getenv_cmd(env, L"ICON_LIST="));
  splitted_list = str_to_array(list, '|');
  while (splitted_list[i])
    {
      verify_file(env, splitted_list[i], config_valid);
      i++;
    }
  *nb_icon = i;
  free_array(splitted_list);
  FreePool(list);
}

void			verify_entry_list(t_env *env, char *config_valid, UINT8 *nb_entry)
{
  CHAR16		*list;
  CHAR16		**splitted_list;
  CHAR16		**tmp_list;
  UINT8			i;

  i = 0;
  log(env, L"Verifying ENTRY_LIST...\n");
  list = strdup(getenv_cmd(env, L"ENTRY_LIST="));
  splitted_list = str_to_array(list, '|');
  while (splitted_list[i])
    {
      tmp_list = str_to_array(splitted_list[i], ':');
      if (arraylen(tmp_list) == 2)
	verify_file(env, tmp_list[1], config_valid);
      else
	{
	  log(env, L"Error on ENTRY_LIST: ");
	  log(env, splitted_list[i]);
	  log(env, L" : please verify the syntax\n");
	  *config_valid = 0;
	}
      free_array(tmp_list);
      i++;
    }
  *nb_entry = i;
  free_array(splitted_list);
  FreePool(list);
}

void			verify_config(t_env *env)
{
  char			config_valid;
  UINT8			nb_icon;
  UINT8			nb_entry;

  config_valid = 1;
  nb_icon = 0;
  nb_entry = 0;
  verify_file(env, getenv_cmd(env, L"DEFAULT_DIR="), &config_valid);
  verify_default_entry(env);
  verify_entry_list(env, &config_valid, &nb_entry);
  if (strcmp(getenv_cmd(env, L"MODE="), L"graphics") == 0)
    {
      verify_images_list(env, &config_valid, &nb_icon);
      if (nb_entry != nb_icon)
	{
	  log(env, L"Error: number of icons and entries are different\n");
	  config_valid = 0;
	}
    }
  // TODO : VERIFY IF ENTRY_LIST IS CORRECT (GOOD NUMBER OF | AND :, file EXISTS, ETC)
  if (config_valid == 0)
    {
      free_array(env->env);
      env->env = str_to_array(BASE_ENV, ':');
      unset(env, L"CONFIG=");
      add_to_env(env, L"CONFIG_ERROR=1");
    }
}

CHAR16			*get_line(UINT8 *buffer, UINTN buffer_size, UINTN *pos)
{
  //static UINTN		pos = 0;
  UINTN			size_read;
  CHAR16		*line;
  UINTN			i;

  size_read = 0;
  i = 0;
  while (buffer[*pos] != '\n' && buffer[*pos])
    {
      (*pos)++;
      size_read++;
    }
  (*pos)++;
  if (*pos > buffer_size)
    return (NULL);
  if (size_read == 0)
    return (strdup(L"\n"));
  line = AllocateZeroPool(size_read * sizeof(CHAR16));
  while (i < size_read)
    {
      line[i] = (CHAR16)buffer[i + (*pos - 1 - size_read)];
      i++;
    }
  line[i] = 0;
  return (line);
}

void			buffer_to_env(t_env *env, t_file *buffer)
{
  CHAR16 		*line;
  UINTN			pos;

  pos = 0;
  line = NULL;
  while ((line = get_line(buffer->buffer, buffer->size, &pos)) != NULL)
    {
      if (line[0] != '\n' && line[0] != '#')
	add_to_env(env, line);
      FreePool(line);
    }
}

t_file			*read_file(EFI_FILE_HANDLE file)
{
  EFI_FILE_INFO		*info;
  t_file		*ret;

  ret = AllocatePool(sizeof(*ret));
  info = LibFileInfo(file);
  ret->size = info->FileSize;
  ret->buffer = (UINT8*)AllocateZeroPool(ret->size);
  file->Read(file, &ret->size, ret->buffer);
  FreePool(info);
  return (ret);
}

void			read_config(t_env *env, CHAR16 *filename)
{
  EFI_HANDLE		base_dir;
  EFI_FILE_HANDLE	file;
  EFI_FILE_HANDLE	current_dir;
  EFI_STATUS		err;
  t_file		*file_buffer;

  base_dir = env->loaded_image->DeviceHandle;
  current_dir = LibOpenRoot(base_dir);
  if (current_dir == NULL)
    {
      Print(L"Error while opening root\r\n");
      return;
    }
  log(env, L"Opening configuration file (");
  log(env, filename);
  err = current_dir->Open(current_dir, &file, filename, EFI_FILE_MODE_READ, 0);
  log(env, L") : done\n");
  if (EFI_ERROR(err))
  {
    log(env, L"Error: Configuration file not found: using default config\n");
    return;
  }
  log(env, L"Reading file content :");
  file_buffer = read_file(file);
  log(env, L" done\nConverting to environement :");
  buffer_to_env(env, file_buffer);
  log(env, L" done\n");
  //log(env, L"Verifying configuration :\n");
  //verify_config(env);
  //log(env, L"Configuration verifying done\n");
  FreePool(file_buffer->buffer);
  FreePool(file_buffer);
  file->Close(file);
}
