/*
** string_utils.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Mon Feb 20 11:11:03 2017 grandr_r
** Last update Wed Jul 26 08:24:36 2017 grandr_r
*/

#include <efi.h>
#include <efilib.h>

void		putchar(CHAR16 c)
{
  CHAR16	str[2];

  str[0] = c;
  str[1] = 0;
  ST->ConOut->OutputString(ST->ConOut, str);
}

void		putnbr(int nb)
{
  if (nb < 0)
    {
      putchar('-');
      putnbr(nb * -1);
    }
  if (nb >= 10)
    {
      putnbr(nb / 10);
      putnbr(nb % 10);
    }
  else
    putchar((CHAR16)(nb + 48));
}

EFI_STATUS      putstring(CHAR16 *str)
{
  int           i;
  EFI_STATUS    err;
  CHAR16        current;

  i = 0;
  current = 0;
  while (str[i])
    {
      current = str[i];
      err = ST->ConIn->Reset(ST->ConIn, FALSE);
      err = ST->ConOut->OutputString(ST->ConOut, &current);
      i++;
    }
  return (err);
}

void            memset(void *str, char c, int len)
{
  CHAR16	*ptr;

  ptr = str;
  while (len--)
    *(ptr++) = c;
}

int		strcmp(CHAR16 *s1, CHAR16 *s2)
{
  int		i;
  int		j;

  i = 0;
  j = 0;
  if (s1 == NULL || s2 == NULL)
    return (0);
  while (s1[i] == s2[j] && s1[i] &&s2[j])
    {
      i++;
      j++;
    }
  if (s1[i] > s2[j])
    return (1);
  else if (s1[i] < s2[j])
    return (-1);
  return (0);
}

int		strncmp(CHAR16 *s1, CHAR16 *s2, int n)
{
  int		i;
  int		j;

  i = 0;
  j = 0;
  if (n == 0)
    return (0);
  while ((s1[i] == s2[j] && s1[i] && s2[j]) && i < n - 1)
    {
      i++;
      j++;
    }
  if (s1[i] > s2[j])
    return (1);
  else if (s1[i] < s2[j])
    return (-1);
  return (0);
}

CHAR16	*strcpy(CHAR16 *dest, CHAR16 *src)
{
  int	i;

  i = 0;
  while (src[i])
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = 0;
  return (dest);
}

CHAR16	*strncpy(CHAR16 *dest, CHAR16 *src, int n)
{
  int	i;

  i = 0;
  while (src[i] && n)
    {
      dest[i] = src[i];
      i++;
      n--;
    }
  dest[i] = 0;
  return (dest);
}

int	strlen(CHAR16 *s)
{
  int	i;

  i = 0;
  while (s[i])
    i++;
  return (i);
}

int	strlen16(CHAR16 *s)
{
  int	i;

  i = 0;
  while (s[i])
    i++;
  return (i * sizeof(CHAR16));
}

void		memcpy(void *dest, void *src, unsigned int len)
{
  CHAR8		*dest_mem;
  const CHAR8	*src_mem;

  dest_mem = dest;
  src_mem = src;
  while (len--)
    *(dest_mem++) = *(src_mem++);
}

CHAR16		*strcat(CHAR16 *dest, CHAR16 *src)
{
  return (strcpy(&dest[strlen(dest)], src));
}

CHAR16		*strdup(CHAR16 *s)
{
  CHAR16	*ret;

  if (s == NULL)
    return (NULL);
  ret = AllocatePool(strlen16(s) + sizeof(CHAR16) * 2);
  memset(ret, 0, strlen(s));
  strcpy(ret, s);
  ret[strlen(s)] = 0;
  return (ret);
}

int     skip_sign(CHAR16 *str)
{
  int   sign;
  int   nbplus;
  int   nbminus;

  nbplus = 0;
  nbminus = 0;
  while ((*str == '-' || *str == '+') && *str)
    {
      if (*str == '+')
        nbplus++;
      if (*str == '-')
        nbminus++;
      str++;
    }
  if (nbminus > nbplus)
    sign = -1;
  else
    sign = 1;
  return (sign);
}

int		atoi(CHAR16 *str)
{
  int   	nb;
  int  		sign;

  if (str == NULL)
    return (0);
  nb = 0;
  sign = skip_sign(str);
  while ((*str == '-' || *str == '+') && *str)
    str++;
  while (*str && *str >= '0' && *str <= '9')
    {
      nb += *str - 48;
      if (*(str + 1))
	nb *= 10;
      str++;
    }
  return (nb * sign);
}

CHAR16 	tolower(CHAR16 c)
{
  if (c >= 'A' && c <= 'Z')
    return (c + 32);
  return (c);
}

char 	nocase_compare(CHAR16 a, CHAR16 b)
{
  int 	gap;

  gap = tolower(a) - tolower(b);
  if (gap == 0)
    return (1);
  return (0);
}

char	match_case(CHAR16 *s1, CHAR16 *s2)
{
  if (*s1 == '\0' && *s2 == '\0')
    return (1);
  else if (nocase_compare(*s1, *s2) && *s1 != '*')
    return (match_case(s1 + 1, s2 + 1));
  else if (*s1 == '*' && *s2 == '*')
    return (match_case(s1 + 1, s2));
  else if (*s2 == '*')
    {
      if (*s1 == '\0')
	return (match_case(s1, s2 + 1));
      else
	return (match_case(s1, s2 + 1) + match_case(s1 + 1, s2));
    }
  else
    return (0);
}

char     match(CHAR16 *s1, CHAR16 *s2)
{
  if (*s1 == '\0' && *s2 == '\0')
    return (1);
  else if (*s1 == *s2 && *s1 != '*')
    return (match(s1 + 1, s2 + 1));
  else if (*s1 == '*' && *s2 == '*')
    return (match(s1 + 1, s2));
  else if (*s2 == '*')
    {
      if (*s1 == '\0')
	return (match(s1, s2 + 1));
      else
	return (match(s1, s2 + 1) + match(s1 + 1, s2));
    }
  else
    return (0);
}

void    remove_letter(char *s, int pos)
{
  while (s[pos] != '\0')
    {
      s[pos] = s[pos + 1];
      pos++;
    }
}

int     only_char(CHAR16 *str, CHAR16 sep)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] != sep)
        return (0);
      i++;
    }
  return (1);
}

CHAR16		*epur_str(CHAR16 *str, char sep)
{
  CHAR16	c;
  int   	c2;
  CHAR16	*dest;

  c = 0;
  c2 = 0;
  if (str == NULL || only_char(str, sep) == 1)
    return (NULL);
  if ((dest = AllocatePool(sizeof(CHAR16) * (strlen(str) + 1))) == NULL)
    return (NULL);
  while (str[c] == sep || str[c] == '\t')
    c++;
  while (c < (strlen(str) - 1))
    {
      if ((str[c] != sep || str[c + 1] != sep) &&
          (str[c] != '\t' || str[c + 1] != '\t'))
        dest[c2++] = str[c];
      c++;
    }
  if (str[c] != sep && str[c] != '\t')
    dest[c2++] = str[c];
  dest[c2] = 0;
  return (dest);
}

