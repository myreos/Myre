/*
** line_utils.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Wed Feb 22 13:49:07 2017 grandr_r
** Last update Thu Apr  6 14:12:01 2017 grandr_r
*/

#include <Myre.h>

void            insert_char(t_shell *shell, CHAR16 c)
{
  CHAR16        newbuff[CMD_LEN];
  int           i;
  int           j;
  int           len;

  i = 0;
  j = 0;
  len = strlen(shell->cmd->line);
  memset(newbuff, 0, CMD_LEN);
  while (i < len + 1)
    {
      if (j != shell->cmd->pos)
        newbuff[j] = shell->cmd->line[i++];
      else
        newbuff[j] = c;
      j++;
    }
  strcpy(shell->cmd->line, newbuff);
}

void            remove_char(t_shell *shell, int pos)
{
  int           i;

  i = pos;
  while (shell->cmd->line[i])
    {
      shell->cmd->line[i] = shell->cmd->line[i + 1];
      i++;
    }
  shell->cmd->line[i] = 0;
}

void            print_line(t_shell *shell)
{
  ST->ConOut->SetCursorPosition(ST->ConOut, strlen(PROMPT), ST->ConOut->Mode->CursorRow);
  Print(shell->cmd->line);
  ST->ConOut->SetCursorPosition(ST->ConOut, strlen(PROMPT) + shell->cmd->pos, ST->ConOut->Mode->CursorRow);

}

void            erase_line(t_shell *shell)
{
  int           i;

  i = 0;
  ST->ConOut->SetCursorPosition(ST->ConOut, 0, ST->ConOut->Mode->CursorRow);
  while (i < strlen(PROMPT) + strlen(shell->cmd->line))
    {
      putchar(' ');
      i++;
    }
  ST->ConOut->SetCursorPosition(ST->ConOut, 0, ST->ConOut->Mode->CursorRow);
  Print(L"%es", PROMPT);
}

