/*
** folder_utils.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Thu Apr  6 08:45:55 2017 grandr_r
** Last update Wed Apr 26 09:44:59 2017 grandr_r
*/

#include <Myre.h>

void		pwd(t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  Print(L"%s\r\n", shell->current_dir);
}

void		get_last_directory(t_shell *shell)
{
  UINT8		i;
  CHAR16	*ptr;

  if (strcmp(shell->current_dir, L"\\") == 0)
    return;
  i = strlen(shell->current_dir) - 1;
  while (shell->current_dir[i] != '\\' && i > 1)
    i--;
  ptr = shell->current_dir;
  shell->current_dir = AllocatePool((i + 1) * sizeof(CHAR16));
  strncpy(shell->current_dir, ptr, i);
  FreePool(ptr);
}

void		change_dir(t_shell *shell, CHAR16 *name)
{
  CHAR16	*ptr;
  UINTN		size;

  ptr = shell->current_dir;
  size = strlen16(ptr) + strlen16(name) + 10 * sizeof(CHAR16);
  shell->current_dir = AllocatePool(size);
  strcpy(shell->current_dir, ptr);
  if (shell->current_dir[strlen(shell->current_dir) - 1] != '\\')
    strcat(shell->current_dir, L"\\");
  strcat(shell->current_dir, name);
  format_path(shell->current_dir);
  FreePool(ptr);
}

char			verif_new_dir(t_shell *shell, CHAR16 *filename)
{
  EFI_FILE_INFO		*info;
  EFI_FILE_HANDLE	file;
  EFI_FILE_HANDLE	current_dir;

  if (!file_exists(shell->env->base_dir, shell->current_dir))
    {
      Print(L"Error: could not cd %s: no such file or directory\r\n", filename);
      return (0);
    }
  current_dir = LibOpenRoot(shell->env->loaded_image->DeviceHandle);
  current_dir->Open(current_dir, &file, shell->current_dir, EFI_FILE_MODE_READ, 0);
  info = LibFileInfo(file);
  file->Close(file);
  if ((info->Attribute & EFI_FILE_DIRECTORY) == 0)
    {
      Print(L"Error: could not cd %s: not a directory\r\n", filename);
      FreePool(info);
      return (0);
    }
  FreePool(info);
  return (1);
}

void		cd(t_shell *shell, CHAR16 **args)
{
  CHAR16	*ptr;

  //TODO : MODIFY ALL OF THIS : CREATE A STRCAT FOLDER + NAME PASSED
  // MODIFY LS TO ALLOW LS DIR_TO_SCAN
  if (arraylen(args) == 1)
    {
      FreePool(shell->current_dir);
      shell->current_dir = strdup(L"\\");
      return;
    }
  if (strcmp(args[1], L".") == 0)
    return;
  ptr = strdup(shell->current_dir);
  if (strcmp(args[1], L"..") == 0)
    {
      get_last_directory(shell);
      return;
    }
  else
    change_dir(shell, args[1]);
  if (!verif_new_dir(shell, args[1]))
    {
      FreePool(shell->current_dir);
      shell->current_dir = ptr;
      return;
    }
  FreePool(ptr);
}
