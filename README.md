I wrote this UEFI bootloader with Intel®’s UEFI library in C from scratch.
I also implemented the CI/CD process so when I push the project, the .EFI file is auto generated and can be tested on Qemu. 

![Screen_Shot_2021-10-29_at_21.38.32](/uploads/2763cb6f2cd417a72dbc5c46873cbb1d/Screen_Shot_2021-10-29_at_21.38.32.png)
